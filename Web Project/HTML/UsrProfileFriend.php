<!doctype html>
<html>
  <head>
        <title>PhotoBasket | Friend Profile</title>
        <link rel = "stylesheet" href = "StyleSheets/bootstrap.min.css">
        <link rel = "stylesheet" href= "StyleSheets/style.css">
        <link rel="stylesheet" href="StyleSheets/animate.min.css">
        <link rel="icon" href="Images/Logo2.ico">
        <script type="text/javascript" src="Scripts/jquery-1.11.3.min.js"></script>   
        <script type="text/javascript" src="Scripts/myJavaScript.js"></script>
        <script type="text/javascript">
            var stat = true;
          
          function myMain()
          {
                $("#settin").click(function(){
                  $("#dropDown").slideToggle();
                  return false;
                })
                $("#head").click(function(){
                  $("#dropDown").slideUp();
                })
                $("#jumb").click(function(){
                  $("#dropDown").slideUp();
                })
                $('.thumbnail').hover(function(){
                    $(this).find('.caption').slideDown(250); //.fadeIn(250)
                },function(){
                    $(this).find('.caption').slideUp(250); //.fadeOut(205)
                });
              
                $('.profileImage').hover(function(){
                    $(this).find('.profileImageCaption').slideDown(250); //.fadeIn(250)
                },function(){
                    $(this).find('.profileImageCaption').slideUp(250); //.fadeOut(205)
                });
              
                $('.circleImg').click(function(){
                    clearUsr();
                    var temp = 0;
                    $(".circleZone").removeClass("label-danger");
                    $(".circleZone").removeClass("label-success");
                    $(".circleZone").removeClass("label-warning");
                    if(this.id == "family"){
                        $("#circleId").text("Family Circle");
                        $(".circleZone").addClass("label-danger");
                        temp = 1;
                    }
                    else if(this.id == "friend"){
                        $("#circleId").text("Friends Circle");
                        $(".circleZone").addClass("label-success");
                        temp = 2;
                    }
                    else{
                        $("#circleId").text("Random Circle");
                        $(".circleZone").addClass("label-warning");
                        temp = 3;
                    }
                    changeImages(temp);
                    $("#CircleInfo").fadeIn();
                    return false;
                })
                
                function changeImages(temp){
                    if(temp == 1){
                        $(".CircleImage").eq(0).attr("src","Images/Users/Family/1.jpg");
                        $(".CircleImage").eq(1).attr("src","Images/Users/Family/2.jpg");
                        $(".CircleImage").eq(2).attr("src","Images/Users/Family/3.jpg");
                        $(".CircleImage").eq(3).attr("src","Images/Users/Family/4.jpg");
                    }
                    else if(temp == 2){
                        $(".CircleImage").eq(0).attr("src","Images/Users/Friends/1.jpg");
                        $(".CircleImage").eq(1).attr("src","Images/Users/Friends/2.jpg");
                        $(".CircleImage").eq(2).attr("src","Images/Users/Friends/3.jpg");
                        $(".CircleImage").eq(3).attr("src","Images/Users/Friends/4.jpg");
                    }
                    else if(temp == 3){
                        $(".CircleImage").eq(0).attr("src","Images/Users/Random/1.jpg");
                        $(".CircleImage").eq(1).attr("src","Images/Users/Random/2.jpg");
                        $(".CircleImage").eq(2).attr("src","Images/Users/Random/3.jpg");
                        $(".CircleImage").eq(3).attr("src","Images/Users/Random/4.jpg");
                    }
                }
                
                $('#addAlbum').click(function(){
                    clearUsr();
                    $("#NewAlbum").fadeIn();
                    
                    return false;
                })
                
                $('.circleImg').hover(function(){
                    if(this.id == "family"){
                        $("#family").attr("src","Images/redCircleF.png")
                    }
                    else if(this.id == "friend"){
                        $("#friend").attr("src","Images/greenCircleF.png")
                    }
                    else{
                        $("#random").attr("src","Images/randomCircleF.png")
                    }
                },function(){
                        if(this.id == "family"){
                        $("#family").attr("src","Images/redCircle.png")
                    }
                    else if(this.id == "friend"){
                        $("#friend").attr("src","Images/greenCircle.png")
                    }
                    else{
                        $("#random").attr("src","Images/randomCircle.png")
                    }
                });
              
          }
            function checkLink(id){
                $("#rightLink").removeClass("activeLink");
                $("#leftLink").removeClass("activeLink");
                $("#CircleInfo").hide();
                $("#NewAlbum").hide();
                if(id == "leftLink"){
                    $("#leftLink").addClass("activeLink");
                    $("#leftLink").css({"color":"white"});
                    $("#rightLink").css({"color":"black"});
                    $("#aboutInfo").fadeOut();
                    $("#albumDetail").fadeIn();
                }
                else{
                    $("#rightLink").addClass("activeLink");
                    $("#leftLink").css({"color":"black"});
                    $("#rightLink").css({"color":"white"});
                    $("#albumDetail").fadeOut();
                    $("#aboutInfo").fadeIn();
                }
                return false;
            }
            
            function edit(){
                if(stat == true){
                    var div = $("<div>");
                    div.css({"margin-bottom":"1px"});
                    var textBox = $("<input>").attr("type","text");
                    textBox.addClass("form-control");
                    textBox.css({"width":"300px","display":"inline","margin-right":"25px"});
                    var save = $("<input>").attr("type","button");
                    save.addClass("form-control");
                    save.val("Save");
                    save.css({"width":"100px","margin-right":"20px","display":"inline"});
                    var cancel = $("<input>").attr("type","button");
                    cancel.addClass("form-control");
                    cancel.val("Cancel");
                    cancel.css({"width":"100px","display":"inline"});
                    var p = $("#status");
                    p.hide();
                    textBox.val(p.text());
                    div.append(textBox);
                    div.append(save);
                    div.append(cancel);
                    $("#statusDiv").append(div);
                    save.bind("click",function(){
                        p.text(textBox.val());
                        div.remove();
                        p.show();
                        stat = true;
                    })
                    cancel.bind("click",function(){
                        div.remove();
                        p.show();
                        stat = true;
                    })
                    stat = false;
                    return false;
                }               
            }
            
            function clearUsr(){
                $("#rightLink").removeClass("activeLink");
                $("#leftLink").removeClass("activeLink");
                $("#rightLink").css({"color":"black"});
                $("#leftLink").css({"color":"black"});
                $("#CircleInfo").hide();
                $("#aboutInfo").hide();
                $("#albumDetail").hide();
                $("#NewAlbum").hide();
            }
              
        </script>
  </head>
    
  <body onload="return myMain()">
    <div class="header" id ="head">
    	<div class="container usrHeader">
        	<div class="row">
                <div class="col-md-5">
                    <a href = "MainPage.html" ><img src="Images/Logo2.png" alt= "Logo not found" class = "logo"></img></a>
                    <h3 class="heading">Photo Basket</h3>
                </div>
                <div class="col-md-4">
                    <div class="form-group has-feedback">
                        <input class="form-control search" type="text" placeholder="Search">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                </div>
                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <a class = "proNav" href = "UsrProfile.html" >Profile</a>
                    <a href="#" class="dropdown-toggle" id= "settin" data-toggle="dropdown"><img src ="Images/settings.png"></a>
                    <ul class="dropdown" id ="dropDown">
                        <li>Settings</li>
                        <li class="dropdownItem">Privacy Settings</li>
                        <li>LogOut</li>   
                    </ul>
                </div>    
            </div>
                       
        </div>
    </div>
    
    
    <div class="jumbotronU" id="jumb">
        <div class="container">
            <div class="profileHeader">
                <a href="#"><div class="profileImage">  
                
                </div></a>
                
                <h1><a href="#" class="profileHeaderLink" >John Snow</a></h1>
                <div id="statusDiv" class="statusClass">
                    <img src="Images/Friend.png" class="friend">
                    <a href="#" ><img src="Images/removeFriend.png" class="removeFriend"></a>
                    <p id = "status" class="statusPara">Winter is Coming</p>
                </div>
                <h5 class="headingUsr">Joined September 2015</h5>
                <div class="profNav">
                    <a href="#" class="profNavLinkLeft activeLink" id="leftLink" onclick="return checkLink(this.id)" style="color:white">Shelf</a>
                    <a href="#" class="profNavLinkRight" id="rightLink" onclick="return checkLink(this.id)">About</a>
                </div>
            </div>  
            <div class="circles">
                <h1>Circles:</h1>
                <div class="subcircle">
                    <a href="#" ><img src="Images/redCircle.png" class="circleImg" id="family"></a>
                    <h3>Family <span class="label label-danger">3</span></h3>
                </div>
                <div class="subcircle">
                    <a href="#" ><img src="Images/greenCircle.png" class="circleImg" id="friend"></a>
                    <h3>Friends <span class="label label-success">7</span></h3>
                </div>
                <div class="subcircle">
                    <a href="#" ><img src="Images/randomCircle.png" class="circleImg" id="random"></a>
                    <h3>Random <span class="label label-warning">15</span></h3> 
                </div>
            </div>
            <div class="albums" id="albumDetail">
                <div class="container">
                    <div class="thumbnail">
                        <div class="caption animated rotateIn">
                            <h4 class="headingAlbums">Album 1</h4>
                            <p>short thumbnail description</p>
                            <a href="" class="label label-success buttonAlbum" >View</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" alt="Server Down">
                    </div>
                    <div class="thumbnail">
                        <div class="caption animated rotateIn">
                            <h4 class="headingAlbums">Album 2</h4>
                            <p>short thumbnail description</p>
                            <a href="" class="label label-success buttonAlbum" >View</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" alt="Server Down">
                    </div>
                    <div class="thumbnail">
                        <div class="caption animated rotateIn">
                            <h4 class="headingAlbums">Album 3</h4>
                            <p>short thumbnail description</p>
                            <a href="" class="label label-success buttonAlbum" >View</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" alt="Server Down">
                    </div>
                    <div class="thumbnail">
                        <div class="caption animated rotateIn">
                            <h4 class="headingAlbums">Album 4</h4>
                            <p>short thumbnail description</p>
                            <a href="" class="label label-success buttonAlbum" >View</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" alt="Server Down">
                    </div>
                </div>
                <a href="#"><img src="Images/next.png" class="nextButtonAlbum"></a>
            </div>
            
            <div class="AboutInfo" id="aboutInfo">
                <h1>About</h1>
                <div class="container">
                    <h4><b>Name: </b><span>John Snow</span></h4>
                    <h4><b>Status: </b><span>Winter is Coming</span></h4>
                    <h4><b>School: </b><span>American Lycetuff</span></h4>
                    <h4><b>College: </b><span>Aitchison College</span></h4>
                    <h4><b>Univeristy: </b><span>Howard University</span></h4>
                    <h4><b>Work: </b><span>Movie Actor</span></h4>
                    <h4><b>Current Address: </b><span>10901 Little Patuxent Pkwy, Columbia, MD 21044, USA</span></h4>
                    <h4><b>Hobbies: </b><span>Cricket, Movies, Gaming</span></h4>
                    <h4><b>Relationship Status: </b><span>Single</span></h4>
                </div>
            </div>
            <div class="CircleInfo" id="CircleInfo">
                <h1 id="circleId">Circle</h1>
                <div class="container">
                    <div class="thumbnail">
                        <div class="caption animated zoomIn">
                            <h4 class="headingAlbums">Arya Stark</h4>
                            <a href="" class="label buttonAlbum circleZone" >Visit</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" class="CircleImage img-responsive" alt="Server Down">
                    </div>
                    <div class="thumbnail">
                        <div class="caption animated zoomIn">
                            <h4 class="headingAlbums">Arya Stark</h4>
                            <a href="" class="label buttonAlbum circleZone" >Visit</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" class="CircleImage img-responsive" alt="Server Down">
                    </div>
                    <div class="thumbnail">
                        <div class="caption animated zoomIn">
                            <h4 class="headingAlbums">Arya Stark</h4>
                            <a href="" class="label buttonAlbum circleZone" >Visit</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" class="CircleImage img-responsive" alt="Server Down">
                    </div>
                    <div class="thumbnail">
                        <div class="caption animated zoomIn">
                            <h4 class="headingAlbums">Arya Stark</h4>
                            <a href="" class="label buttonAlbum circleZone" >Visit</a>
                        </div>
                        <img src="http://lorempixel.com/400/300/technics/2/" class="CircleImage img-responsive" alt="Server Down">
                    </div>
                </div>
                <a href="#"><img src="Images/next.png" class="nextButtonAlbum"></a>
            </div>
            
            
            <div class="newAlbum" id="NewAlbum">
                <h1>New Album</h1>
                <div class="container">
                    <div class="thumbnail newAlbumImage">
                        <div class="caption animated jello">
                            <h4 class="headingAlbums">Cover Image</h4>
                            <a href=""><img src="Images/AddImage.png" width="50px" height="50px"></a>
                        </div>
                        <img src="Images/profilePicture.jpg" alt="Server Down">
                    </div>
                    <div class="newAlbumSec">
                        <form class="form-horizontal" role="form">
                            <div class="form-group has-feedback">
                                <label class="col-md-3 control-label">Title:</label>
                                <div class="col-md-5">
                                    <input class="form-control" id="title" type="text" placeholder="Enter Title">
                                </div>
                                <img class="col-md-2 col-md-offset-2" src="Images/design.png" width="30px" height="50px">
                            </div>
                            <div class="form-group has-feedback">
                                <label class="col-md-3 control-label">Description:</label>
                                <div class="col-md-7">
                                    <input class="form-control" id="decript" type="text" placeholder="Description">
                                </div>
                            </div>
                            <div class="form-group has-feedback">
                                <a href="#" class="col-md-3 col-md-offset-8 uploadBtn">Upload</a>
                            </div>  
                        </form>
                    </div>
                    
                    <div class="uploader">
                        <div class="thumbnail newAlbumImage">
                            <div class="caption uploaderCaption animated jello">
                                <a href=""><img src="Images/upload.png" width="50px" height="50px" style="margin-top:30px;"></a>
                            </div>
                            <img src="Images/profilePicture.jpg" alt="Server Down">
                        </div>
                        <div class="thumbnail newAlbumImage">
                            <div class="caption uploaderCaption animated jello">
                                <a href=""><img src="Images/upload.png" width="50px" height="50px" style="margin-top:30px;"></a>
                            </div>
                            <img src="Images/profilePicture.jpg" alt="Server Down">
                        </div>
                        <div class="thumbnail newAlbumImage">
                            <div class="caption uploaderCaption animated jello">
                                <a href=""><img src="Images/upload.png" width="50px" height="50px" style="margin-top:30px;"></a>
                            </div>
                            <img src="Images/profilePicture.jpg" alt="Server Down">
                        </div>
                        <div class="thumbnail newAlbumImage">
                            <div class="caption uploaderCaption animated jello">        
                                <a href=""><img src="Images/upload.png" width="50px" height="50px" style="margin-top:30px;"></a>
                            </div>
                            <img src="Images/profilePicture.jpg" alt="Server Down">
                        </div>
                    </div> 
                    <a href="#"><img src="Images/next.png" class="nextUploader"></a>
                </div>
            </div>
            
        </div>
    </div>
  </body>
</html>