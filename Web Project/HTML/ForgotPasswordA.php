<?php
session_start();

	if(isset($_SESSION["user"])== false)
	{
		header('Location:ForgotPassword.php');
	}

	
?>


<!doctype html>
<html>
  <head>
        <title>PhotoBasket | Change Password</title>
        <link rel = "stylesheet" href = "StyleSheets/bootstrap.min.css">
        <link rel = "stylesheet" href= "StyleSheets/style.css">
        <link rel="icon" href="Images/Logo2.ico">
        <script type="text/javascript" src="Scripts/jquery-1.11.3.min.js"></script>   
        <script type="text/javascript" src="Scripts/myJavaScript.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#fError").hide();    
            })
            
            function change(){
                var pwd = $("#fPass");
                var cPwd = $("#fVPas");
                pwd.closest("div").removeClass("has-error");
                cPwd.closest("div").removeClass("has-error");
                pwd.closest("div").find("span").addClass("hidden");
                cPwd.closest("div").find("span").addClass("hidden");
                if(pwd.val() == ""){
                    $("#fError").text("Invalid Password");
                    $("#fError").show();
                    pwd.closest("div").addClass("has-error");
                    pwd.closest("div").find("span").removeClass("hidden");
                }
                else if(pwd.val() != cPwd.val()){
                    $("#fError").text("Password Mismatch");
                    $("#fError").show();
                    pwd.closest("div").addClass("has-error");
                    pwd.closest("div").find("span").removeClass("hidden");
                    cPwd.closest("div").addClass("has-error");
                    cPwd.closest("div").find("span").removeClass("hidden");
                }
                else{
                    pwd.prop("disabled", true);
                    cPwd.prop("disabled", true);
                    $("#fError").removeClass("text-danger");
                    $("#fError").addClass("text-success");
                    $("#fError").text("Password Changed (:");
                    $("#fError").show();
                }
                
              
				var data = {"action": "forgotpasswordA", 'password':pwd.val()};
				var settings= {
				type: "POST",
				dataType: "json",
				url: "response.php",
				data: data,
				success: function(response) {
						if(response.data == "true")
						alert("password changed");
						
					},
					error: function (err, type, httpStatus) {
						alert(httpStatus);
					}
				};
			
				$.ajax(settings);
			
                
                
            }
      
        </script>
  </head>
    
  <body>
    <div id="divoverlay" class="overlay">
    </div>
    <div class="header">
    	<div class="container">
        	<div class="row">
        		<a href = "MainPage.php" ><img src="Images/Logo2.png" alt= "Logo not found" class = "logo"></img></a>
                <h3 class="heading">Photo Basket</h3>
                <a href="#" class="btn1" onclick="return loginCnt()">Login</a>
                <a href="SignUp.php" class="btn2">Join</a>
            </div>
        </div>
    </div>
    
    <div class="jumbotronF">
        <div class="container" id = "jum">
            <h1>Change Password</h1>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">New Password:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="fPass" type="password" placeholder="Password">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Verify Password:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="fVPas" type="password" placeholder="Password">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-md-offset-7">
                        <input class="btn btn-default btn-md" id="chng" type=button value = "Change Password" onclick = "return change()">
                    </div>
                </div>
                <label class="col-md-3 col-md-offset-4 text-danger control-label" id = "fError">Invalid Password</label>
            </form>
        </div>
    </div>
     <center>
        <div class="signIn" id = "login"> 
            <a href = "#" onclick="return closeCnt()"><img src ="Images/cross.png"></a>
            <h3>Login:</h3>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrName" type="text" placeholder="Email">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrNameLab">Invalid Email</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrPwd" type="password" placeholder="Password">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrPwdLab">Invalid Password</span></label>
                </div>
                <div class="form-group">
                    
                </div>
                <div class="form-group">
                    <a class = "col-sm-6" href="ForgotPassword.php">Forgot Password</a>
                    <div class="col-sm-3">
                        <input class="btn btn-default btn-md" id="submit" type=button value = "SignIn" onclick = "return loginBtnFun()">
                    </div>
                </div>
				
				<div class="form-group">
					<div class="col-sm-3">
                    </div>
                    <div class="col-sm-5">
                        <span class="text-danger hidden" id="lgnLab">Invalid Username and Password</span>
                    </div>
                </div>
				
            </form>
        </div>
    </center>
    
    <div class ="footer">
        <div class = "container">
            <h4>&copy; Powered by CMD</h4>
            <a class = "footerNav" href="TermsPolicy.php">Terms and Conditions</a>
        </div>      
    </div>
  </body>
</html>