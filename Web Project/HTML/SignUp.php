<!doctype html>
<html>
  <head>
        <link rel="icon" href="Images/Logo2.ico">
        <script type="text/javascript" src="Scripts/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="Scripts/myJavaScript.js"></script>
        <script type="text/javascript" src="Scripts/jquery-ui.min.js"></script>
        <link rel = "stylesheet" href = "StyleSheets/bootstrap.min.css">
        <link rel = "stylesheet" href= "StyleSheets/style.css">
        <link rel=  "stylesheet" href= "StyleSheets/jquery-ui.min.css">
        <title>PhotoBasket | SignUp</title>
        <script type="text/javascript">
            $(function() {       
                $( "#datepicker" ).datepicker();
            });
            function myMain(){
                hideLabels();
            }
			
			
            
            function hideLabels(){
                var fName = $("#fLab");
                var lName = $("#lLab");
                var email = $("#emailLab");
                var cEmail = $("#cEmailLab");
                var pwd = $("#pwdLab");
                var cPwd = $("#cPwdLab");
                var country = $("#coun");
                var date = $("#dateLab");
                lName.hide();
                fName.hide();
                email.hide();
                cEmail.hide();
                pwd.hide();
                cPwd.hide();
                country.hide();
                date.hide();
                fName.closest("div").removeClass("has-error");
                lName.closest("div").removeClass("has-error");
                email.closest("div").removeClass("has-error");
                pwd.closest("div").removeClass("has-error");
                $("#country").closest("div").removeClass("has-error");
                date.closest("div").removeClass("has-error");
                fName.closest("div").find("span").addClass("hidden");
                lName.closest("div").find("span").addClass("hidden");
                email.closest("div").find("span").addClass("hidden");
                cEmail.closest("div").find("span").addClass("hidden");
                pwd.closest("div").find("span").addClass("hidden");
                cPwd.closest("div").find("span").addClass("hidden");
                date.closest("div").find("span").addClass("hidden");
                $("#cEmail").closest("div").removeClass("has-error");
                $("#cPwd").closest("div").removeClass("has-error");
            }
            
            function validation()
            {
                hideLabels();
                var fName = $("#fName");
                var lName = $("#lName");
                var email = $("#email");
                var cEmail = $("#cEmail");
                var pwd = $("#pwd");
                var cPwd = $("#cPwd");
                var dpicker = $("#datepicker");
                var country = $("#country");
				var gender = $("input[name=optRadio]:checked").val();
			
                var status = true;
                if(fName.val() == ""){
                    $("#fLab").show();
                    fName.next().removeClass("hidden");
                    $("#fLab").closest("div").addClass("has-error");
                    status = false;
                }
                if(lName.val() == ""){
                    $("#lLab").show();
                    lName.next().removeClass("hidden");
                    $("#lLab").closest("div").addClass("has-error");
                    status = false;
                }
                 if(email.val() == ""){
                    $("#emailLab").text("Invalid email");
                    $("#emailLab").show();
                     email.next().removeClass("hidden");
                    $("#emailLab").closest("div").addClass("has-error");
                     status = false;
                }
                else if(email.val() != cEmail.val()){
                    $("#emailLab").text("Email Mismatch");
                    $("#emailLab").show();
                     email.next().removeClass("hidden");
                    $("#emailLab").closest("div").addClass("has-error");     
                     cEmail.next().removeClass("hidden");
                    $("#cEmail").closest("div").addClass("has-error");
                    status = false;
                }
                if(pwd.val() == ""){
                    $("#pwdLab").text("Invalid Password");
                    $("#pwdLab").show();
                     pwd.next().removeClass("hidden");
                    $("#pwdLab").closest("div").addClass("has-error");
                    status = false;
                }
                else if(pwd.val() != cPwd.val()){
                    $("#pwdLab").text("Password Mismatch");
                    $("#pwdLab").show();
                     pwd.next().removeClass("hidden");
                    $("#pwdLab").closest("div").addClass("has-error");     
                     cPwd.next().removeClass("hidden");
                    $("#cPwd").closest("div").addClass("has-error");
                    status = false;
                }
                if(dpicker.val() == ""){
                    $("#dateLab").show();
                    dpicker.next().removeClass("hidden");
                    $("#dateLab").closest("div").addClass("has-error");
                    status = false;
                }
                if(country.find(":selected").text() == ""){
                    $("#coun").show();
                    country.closest("div").addClass("has-error");
                    status = false;
                }
                
                if(status == true){
                    var data = {"action": "signup",'fname':fName.val(),'lname':lName.val(),'email':email.val(),'password':pwd.val(),"date":dpicker.val(),'country':country.find(":selected").text(),'gender':gender};
					
                    var settings= {
					type: "POST",
					dataType: "json",
					url: "response.php",
					data: data,
					success: function(response) {
					
							if(response.data == "true")
                            {
                                alert("Congradulations! Your account have been created.");
                            }
                        else
                        {
                            alert("Invalid Email, User already exists.");
                        }
                        
						},
					error: function (err, type, httpStatus) {
							alert(httpStatus);
						}
                        
					};
			
					$.ajax(settings);
				}
			}
        </script>
        
  </head>
    
  <body onload="return myMain()">
    <div id="divoverlay" class="overlay">
    </div>
      
    <div class="header">
    	<div class="container">
        	<div class="row">
        		<a href = "MainPage.php" ><img src="Images/Logo2.png" alt= "Logo not found" class = "logo"></img></a>
                <h3 class="heading">Photo Basket</h3>
                <a href="#" class="btn1" onclick="return loginCnt()">Login</a>
            </div>
        </div>
    </div>
    <div class="jumbotronS">
        <div class="container" id = "jum">
            <h1>Personal Information</h1>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">First Name:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="fName" type="text" placeholder="First Name">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "fLab">Invalid First Name</label>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Last Name:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="lName" type="text" placeholder="Last Name">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "lLab">Invalid Last Name</label>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Email:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="email" type="text" placeholder="SansaStark@live.com">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "emailLab">Incorrect Email</label>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Confirm Email:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="cEmail" type="text" placeholder="SansaStark@live.com">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "cEmailLab">Incorrect Email</label>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Password:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="pwd" type="password" placeholder="Enter Password">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "pwdLab">Password Mismatch</label>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Confirm Password:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="cPwd" type="password" placeholder="Confirm Password">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "cPwdLab">Password Mismatch</label>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Date:</label>
                    <div class="col-md-5">
                        <input type="text" id="datepicker" class="form-control" placeholder="Pick date">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "dateLab">Invalid Date</label>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">Gender:</label>
                    <div class="col-md-2">
                        <label><input  id="oRad" type="radio" value="0" name="optRadio" checked>&nbsp;Male</label>
                    </div>
                    <div class="col-md-2">
                        <label><input  id="oRad" type="radio" value="1" name="optRadio">&nbsp;Female</label>
                    </div>
                    <div class="col-md-2">
                        <label><input  id="oRad" type="radio" value="2" name="optRadio">&nbsp;Not Specified</label>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Select Country:</label>
                    <div class="col-md-5">
                        <select class="form-control" id="country">
                            <option></option>
                            <option>Pakistan</option>
                            <option>Saudi Arabia</option>
                            <option>India</option>
                            <option>China</option>
                            <option>America</option>
                            <option>Japan</option>
                            <option>Germany</option>
                            <option>Russia</option>
                            <option>Romania</option>
                            <option>Dubai</option>
                            <option>Africa</option>
                            <option>Palestain</option>
                            <option>Malaysia</option>
                        </select>
                    </div>
                    <label class="col-md-2 text-danger control-label" id = "coun">Invalid Country</label>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-md-offset-7">
                        <input class="btn btn-default btn-md" id="submit" type=button value = "SignUp" onclick = "return validation()">
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    
    <center>
        <div class="signIn" id = "login"> 
            <a href = "#" onclick="return closeCnt()"><img src ="Images/cross.png"></a>
            <h3>Login:</h3>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrName" type="text" placeholder="Email">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrNameLab">Invalid Email</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrPwd" type="password" placeholder="Password">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrPwdLab">Invalid Password</span></label>
                </div>
                <div class="form-group">
                    
                </div>
                <div class="form-group">
                    <a class = "col-sm-6" href="ForgotPassword.php">Forgot Password</a>
                    <div class="col-sm-3">
                        <input class="btn btn-default btn-md" id="submit" type=button value = "SignIn" onclick = "return loginBtnFun()">
                    </div>
                </div>
				
				<div class="form-group">
					<div class="col-sm-3">
                    </div>
                    <div class="col-sm-5">
                        <span class="text-danger hidden" id="lgnLab">Invalid Username and Password</span>
                    </div>
                </div>
				
            </form>
        </div>
    </center>
    
    <div class ="footer">
        <div class = "container">
            <h4>&copy; Powered by CMD</h4>
            <a class = "footerNav" href="TermsPolicy.php">Terms and Conditions</a>
        </div>      
    </div>
  </body>
</html>