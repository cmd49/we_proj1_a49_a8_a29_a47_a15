<?php include('conn.php'); ?>

<!doctype html>
<html>
  <head>
        <title>PhotoBasket</title>
        <link rel = "stylesheet" href = "StyleSheets/bootstrap.min.css">
        <link rel = "stylesheet" href= "StyleSheets/style.css">
        <link rel="icon" href="Images/Logo2.ico">
        <script type="text/javascript" src="Scripts/jquery-1.11.3.min.js"></script>   
        <script type="text/javascript" src="Scripts/myJavaScript.js"></script>
        <script type="text/javascript">
          
          function myMain()
          {
              setInterval(slideshow,3000);      
          }
          
          function buttonCont(ide)
          {
                var sl;
                if(ide == "link1"){
                    document.getElementById("mySlide").style.backgroundImage = "url(./Images/slide1.jpg)";
                    sl = document.getElementById("link2");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link3");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link1");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link1");
                    sl.setAttribute("class", "active");
                    slide = 1;
                }
                else if(ide == "link2"){
                    document.getElementById("mySlide").style.backgroundImage = "url(./Images/slide2.jpg)";
                    sl = document.getElementById("link1");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link3");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link2");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link2");
                    sl.setAttribute("class", "active");
                    slide = 2;
                }
              else if(ide == "link3"){
                    document.getElementById("mySlide").style.backgroundImage = "url(./Images/slide3.jpg)";
                    sl = document.getElementById("link2");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link1");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link3");
                    sl.setAttribute("class", "");
                    sl = document.getElementById("link3");
                    sl.setAttribute("class", "active");
                    slide = 3;
              }
          } 
      </script>
  </head>
    
  <body onload="return myMain()">
    
    <div id="divoverlay" class="overlay">
    </div>
      
    <div class="header">
    	<div class="container">
        	<div class="row">
        		<a href = "MainPage.php" ><img src="Images/Logo2.png" alt= "Logo not found" class = "logo"></img></a>
                <h3 class="heading">Photo Basket</h3>
                <a href="#" class="btn1" onclick="return loginCnt()">Login</a>
                <a href="SignUp.php" class="btn2">Join</a>
            </div>
        </div>
    </div>
    
    <div id = "mySlide" class="jumbotron">
        
        <div id="myCarousel" class="carousel slide">
            <ol class="carousel-indicators">
                <li id ="link1" class="active" onclick="return buttonCont(this.id)"></li>
                <li id ="link2" onclick="return buttonCont(this.id)"></li>
                <li id ="link3" onclick="return buttonCont(this.id)"></li>
            </ol>
        </div>
        
    </div>
     <center>
        <div class="signIn" id = "login"> 
            <a href = "#" onclick="return closeCnt()"><img src ="Images/cross.png"></a>
            <h3>Login:</h3>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrName" type="text" placeholder="Email">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrNameLab">Invalid Email</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrPwd" type="password" placeholder="Password">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrPwdLab">Invalid Password</span></label>
                </div>
                <div class="form-group">
                    
                </div>
                <div class="form-group">
                    <a class = "col-sm-6" href="ForgotPassword.php">Forgot Password</a>
                    <div class="col-sm-3">
                        <input class="btn btn-default btn-md" id="submit" type=button value = "SignIn" onclick = "return loginBtnFun()">
                    </div>
                </div>
				
				<div class="form-group">
					<div class="col-sm-3">
                    </div>
                    <div class="col-sm-5">
                        <span class="text-danger hidden" id="lgnLab">Invalid Username and Password</span>
                    </div>
                </div>
				
            </form>
        </div>
    </center>
    
    <div class ="footer">
        <div class = "container">
            <h4>&copy; Powered by CMD</h4>
            <a class = "footerNav" href="TermsPolicy.php">Terms and Conditions</a>
        </div>      
    </div>
  </body>
</html>