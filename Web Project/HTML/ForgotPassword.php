<!doctype html>
<html>
  <head>
        <title>PhotoBasket | Forgot Password</title>
        <link rel = "stylesheet" href = "StyleSheets/bootstrap.min.css">
        <link rel = "stylesheet" href= "StyleSheets/style.css">
        <link rel="icon" href="Images/Logo2.ico">
        <script type="text/javascript" src="Scripts/jquery-1.11.3.min.js"></script>   
        <script type="text/javascript" src="Scripts/myJavaScript.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#fError").hide();
                $("#fSuccess").hide();
                $("#fVCode").hide();
                $("#verify").hide();
                $("#lab").hide();
            })
            
            function code(){
                $("#fError").hide();
                $("#fSuccess").hide();
                $("#fEmail").next().addClass("hidden");
                $("#fEmail").closest("div").removeClass("has-error");
                $("#fEmail").next().next().addClass("hidden");
                $("#fEmail").closest("div").removeClass("has-success");
                if($("#fEmail").val() == ""){
                    $("#fError").show();
                    $("#fError").text("Invalid Email");
                    $("#fEmail").next().removeClass("hidden");
                    $("#fEmail").closest("div").addClass("has-error");
                }
                else{
                    $("#fSuccess").show();
                    $("#sendCode").val("Resend Code");
                    $("#fEmail").next().next().removeClass("hidden");
                    $("#fEmail").closest("div").addClass("has-success");
                    $("#fEmail").prop("disabled", true);
                    $("#fVCode").show();
                    $("#verify").show();
                    $("#lab").show();
                }
                var email = ("#fEmail");
                function emailVerif(){
				var data = {"action": "forgotpassword", 'email':email.val()};
				var settings= {
				type: "POST",
				dataType: "json",
				url: "response.php",
				data: data,
				success: function(response) {
						
						if(response.data == "false")
                        {
                            alert("Invalid Email, please enter a Valid Email.");
                        }
						
					},
					error: function (err, type, httpStatus) {
						alert(httpStatus);
					}
				};
			
				$.ajax(settings);
			}
            }
            
            function verifyF(){
              
                $("#fError").hide();
                $("#fVCode").closest("div").removeClass("has-error");
                $("#fVCode").closest("div").find("span").addClass("hidden");
                if($("#fVCode").val() == ""){
                    $("#fError").show();
                    $("#fError").text("Invalid Code");
                    $("#fVCode").next().removeClass("hidden");
                    $("#fVCode").closest("div").addClass("has-error");
                }
                else{
                    window.location.href = "ForgotPasswordA.php";
                }
            }
        </script>
  </head>
    
  <body>
    <div id="divoverlay" class="overlay">
    </div>
    <div class="header">
    	<div class="container">
        	<div class="row">
        		<a href = "MainPage.php" ><img src="Images/Logo2.png" alt= "Logo not found" class = "logo"></img></a>
                <h3 class="heading">Photo Basket</h3>
                <a href="#" class="btn1" onclick="return loginCnt()">Login</a>
                <a href="SignUp.php" class="btn2">Join</a>
            </div>
        </div>
    </div>
    
    <div class="jumbotronF">
        <div class="container" id = "jum">
            <h1>Forgot Password</h1>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label">Email:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="fEmail" type="text" placeholder="AryaStark@gmail.com">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                        <span class="glyphicon glyphicon-ok form-control-feedback hidden"></span>
                    </div>
                    <label class="col-md-2 text-success control-label" id = "fSuccess">Code Sent!</label>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-md-offset-7">
                        <input class="btn btn-default btn-md" id="sendCode" type=button value = "Send Code" onclick = "return code()">
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label class="col-md-4 control-label" id = "lab">Code:</label>
                    <div class="col-md-5">
                        <input class="form-control" id="fVCode" type="text" placeholder="Verification Code">
                        <span class="glyphicon glyphicon-remove form-control-feedback hidden"></span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-md-offset-7">
                        <input class="btn btn-default btn-md" id="verify" type=button value = "Verify" onclick = "return verifyF()">
                    </div>
                </div>
                <label class="col-md-2 col-md-offset-4 text-danger control-label" id = "fError">Invalid Code</label>
            </form>
        </div>
    </div>
     <center>
        <div class="signIn" id = "login"> 
            <a href = "#" onclick="return closeCnt()"><img src ="Images/cross.png"></a>
            <h3>Login:</h3>
            <form class="form-horizontal" role="form">
                <div class="form-group has-feedback">
                    <label class="col-sm-3 control-label">Email:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrName" type="text" placeholder="Email">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrNameLab">Invalid Email</label>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-6">
                        <input class="form-control" id="usrPwd" type="password" placeholder="Password">
                    </div>
                    <label class="col-sm-2 text-danger control-label" id = "usrPwdLab">Invalid Password</span></label>
                </div>
                <div class="form-group">
                    
                </div>
                <div class="form-group">
                    <a class = "col-sm-6" href="ForgotPassword.php">Forgot Password</a>
                    <div class="col-sm-3">
                        <input class="btn btn-default btn-md" id="submit" type=button value = "SignIn" onclick = "return loginBtnFun()">
                    </div>
                </div>
				
				<div class="form-group">
					<div class="col-sm-3">
                    </div>
                    <div class="col-sm-5">
                        <span class="text-danger hidden" id="lgnLab">Invalid Username and Password</span>
                    </div>
                </div>
				
            </form>
        </div>
    </center>
    
    <div class ="footer">
        <div class = "container">
            <h4>&copy; Powered by CMD</h4>
            <a class = "footerNav" href="TermsPolicy.php">Terms and Conditions</a>
        </div>      
    </div>
  </body>
</html>